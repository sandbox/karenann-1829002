<?php

/**
 * Implement hook_update_projects_alter().
 */
function css_injector_images_update_projects_alter(&$projects) {
  unset($projects['css_injector_images']);
}

/**
 * Implement hook_permission().
 */
function css_injector_images_permission() {
  return array(
    'administer css injector images config' => array('title' => t('Administer CSS Injector Images Configuarion'), 'description' => t('Allow administration of the configuration settings for CSS Injector Images.'), ),
  );
}

/**
 * Implements hook_menu().
 */
function css_injector_images_menu() {

  $items = array();

  if (module_exists('imce')) {

    $items['admin/config/development/css-injector/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    $items['admin/config/development/css-injector/imce'] = array(
      'title' => 'File browser',
      'page callback' => 'css_injector_images_imce_user_page',
      'access callback' => 'css_injector_images_imce_page_access',
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
    );

  }

  $items['admin/config/development/css-injector/config/images'] = array(
    'title' => 'Image Config',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('css_injector_images_config'),
    'access arguments' => array('administer css injector images config'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );

  return $items;
}

/**
 *
 */
function css_injector_images_config() {

  $form = array();

  $form['css_injector_images_upload_validators'] = array(
    '#type' => 'textfield',
    '#title' => t('Filtypes Allowed'),
    '#default_value' => variable_get('css_injector_images_upload_validators', 'png gif jpg'),
    '#description' => t('Setting format: png gif jpg'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_alter().
 */
function css_injector_images_form_alter(&$form, &$form_state, $form_id) {

  // Hooking into IMCE's profile form to assist with injected image management
  if ($form_id == 'imce_profile_form') {

    // IMCE stores profile values in $form_state['profile']
    // ['ciitab'] will not exist upon initial profile load
    $form_state['profile']['ciitab'] = (isset($form_state['profile']['ciitab'])) ? $form_state['profile']['ciitab'] : 0;

    // Source form doesn't have explicit weights
    // Instead of setting weights, rebuilding array
    $rebuild = array();

    foreach ($form['profile'] as $key => $val) {

      // Each item is automatically included
      $rebuild[$key] = $val;

      // We want to add our field after the usertab
      if ($key == 'usertab') {
        $rebuild['ciitab'] = array(
          '#type' => 'checkbox',
          '#title' => t('Display file browser tab in the CSS Injector'),
          '#default_value' => $form_state['profile']['ciitab'],
          '#description' => t('User 1 will always see tab'),
        );
      }
    }

    // We are replacing the default form with our modded one
    $form['profile'] = $rebuild;

    // Our new element will automatically be saved in
    // imce/inc/imce.admin.inc - function imce_update_profiles
    // No further action need be made
  }

  // Hooking into IMCE's deletion to assist with image management
  if ($form_id == 'imce_fileop_form') {
    $form['fset_delete']['delete']['#submit'] = array('css_injector_images_imce_delete_submit', 'imce_delete_submit');
  }

  // Hooking into IMCE's upload to assist with image management
  if ($form_id == 'imce_upload_form') {
    $form['fset_upload']['upload']['#submit'][] = 'css_injector_images_imce_upload_submit';
  }

  if ($form_id == "css_injector_edit") {

    // The id of the css injection we are currently on.
    $crid = (isset($form['crid']) && isset($form['crid']['#value'])) ? $form['crid']['#value'] : 0;

    // Fetch all image information, all resources are pooled.
    $query = db_select('file_usage', 'u');
    $query->join('file_managed', 'm', 'u.fid = m.fid');
    $query->fields('u', array('fid'));
    $query->fields('m', array('uri'));
    $query->condition('u.module', 'css_injector_images', '=');

    $result = $query
      ->execute()
      ->fetchAll();

    // Add a blank that will serve as the single add file field.
    array_push($result, new stdClass());

    // We need to keep track of how many items we have.
    $count = (!isset($form_state['input']['css_injector_images_count']))
      ? count($result)
      : $form_state['input']['css_injector_images_count'];

    $form['css_injector_images_count'] = array('#type' => 'hidden', '#value' => $count);

    if ($count > 0) {

      // Holding upload fields in a fieldset for cleanliness
      $form['css_injector_images_images'] = array(
        '#title' => t('Uploaded Images'),
        '#description' => t('Permitted file types: @filetypes', array('@filetypes' => variable_get('css_injector_images_upload_validators', 'png gif jpg'))),
        '#type' => 'fieldset',
        '#collapsed' => FALSE,
        '#collapsible' => TRUE,
        '#weight' => -50,
        '#attributes' => array('class' => array('css-injector-images-images')),
      );

      // Create the image fields.
      for ($i = 0; $i < $count; $i++) {

        $vars = array(
          'ct' => $i,
          'fid' => ((!empty($result[$i]) && isset($result[$i]->fid)) ? $result[$i]->fid : 0),
          'uri' => ((!empty($result[$i]) && isset($result[$i]->uri)) ? $result[$i]->uri : ''),
          'expose' => ($i == $count - 1),
        );

        $field = css_injector_images_form_fields('css_injector_images_images', $vars);

        $form['css_injector_images_images']['css_injector_images_image_' . $i] = $field;
      }
    }

    // We need to rewrite the description when we upload or remove images.
    if ($form_state['rebuild']) {

      $parents = $form_state['triggering_element']['#array_parents'];
      $bttnkey = array_pop($parents);
      $element = drupal_array_get_nested_value($form, $parents);

      if (strstr($element['#name'], 'css_injector_images_image_')) {
        switch ($bttnkey) {
          case 'remove_button':
            break;
          case 'upload_button':
            if (intval($form_state['values'][$element['#name']]) > 0) {
              $vars  = array(
                'ct' => $count + 1,
                'expose' => 1
              );
              $field = css_injector_images_form_fields('css_injector_images_images', $vars);
              $form['css_injector_images_images']['css_injector_images_image_' . $count] = $field;
              $form['css_injector_images_count']['#value'] = $count + 1;
            }
            break;
        }
      }
    }

    // Ensuring that these appear at the top
    $form['title']['#weight'] = -1000;
    $form['css_text']['#weight'] = -999;

    // Custom submit handler on the CSS Injector created buttons.
    $form['buttons']['save']['#submit'][] = 'css_injector_images_form_save';
    $form['buttons']['save_and_continue']['#submit'][] = 'css_injector_images_form_save';

    // Custom class for styling purposes
    $form['#attributes']['class'][] = 'css-injector-images-form';

    // Custom styles for our modified form
    $form['#attached'] = array(
      'css' => array(
        'type' => 'file',
        'data' => drupal_get_path('module', 'css_injector_images') . '/css/css_injector_images_form.css',
      ),
    );
  }
}

/**
 * Upload(ed) image field structure
 */
function css_injector_images_form_fields($id, $vars = array()) {

  switch ($id) {

    case 'css_injector_images_images':

      if (intval($vars['ct']) < 0)
        return array();

      $field = array(
        '#title' => t(''),
        '#type' => 'managed_file',
        '#name' => 'css_injector_images_image_' . $vars['ct'],
        '#size' => 60,
        '#weight' => -($vars['ct']),
        '#expose' => (isset($vars['expose']) ? $vars['expose'] : 0),
        '#default_value' => ((isset($vars['fid']) && intval($vars['fid']) > 0) ? intval($vars['fid']) : 0),
        '#process' => array('file_managed_file_process', 'css_injector_images_file_managed_file_process'),
        '#pre_render' => array('file_managed_file_pre_render', 'css_injector_images_file_managed_file_pre_render'),
        '#progress_indicator' => variable_get('css_injector_images_progress_indicator', 'bar'),
        '#upload_location' => variable_get('css_injector_images_upload_location', 'public://css_injector_images_image/'),
        '#upload_validators' => array(
          'file_validate_extensions' => array(variable_get('css_injector_images_upload_validators', 'png gif jpg'))
        ),
      );
      return $field;
  }
  return array();
}

/**
 * Remove AJAX from uploader to eliminate js introduced anomalies.
 */
function css_injector_images_file_managed_file_process($element, &$form_state, $form) {
  unset($element['upload_button']['#ajax']);
  unset($element['remove_button']['#ajax']);
  return $element;
}

/**
 * Intended to run with each managed_file image uploader
 */
function css_injector_images_file_managed_file_pre_render($element) {
  // If we have a file, we want to show the path.
  if (!empty($element['#value']['fid'])) {
    if (isset($element['#file']->uri)) {
      $element['#description'] = file_create_url($element['#file']->uri);
    }
  }
  // Else, we want to clear the description and hide the field
  else {
    $element['#description'] = '';
    if (!isset($element['#expose']) || !$element['#expose']) {
      $element['#attributes']['class'][] = 'hidden';
    }
  }
  return $element;
}

/**
 *
 */
function css_injector_images_form_save($form, &$form_state) {

  $crid = $form_state['values']['rule']['crid'];

  $files = db_select('file_usage', 'fu')
    ->fields('fu', array('fid', 'count'))
    ->condition('module', 'css_injector_images', '=')
    ->execute()
    ->fetchAllKeyed(0, 1);

  // Loop and get values
  foreach ($form_state['values'] as $key => $value) {
    if (strstr($key, 'css_injector_images_image_')) {
      if (intval($value) > 0) {
        if (isset($files[$value])) unset($files[$value]);
        $file = file_load($value);
        if ($file->status == 0) {
          $file->status = FILE_STATUS_PERMANENT;
          file_save($file);
          file_usage_add($file, 'css_injector_images', 'file', $crid);
          drupal_set_message(t('%filename has been added to the CSS Injector repository.', array('%filename' => $file->filename)));
        }
      }
    }
  }

  // Loop through our files list. Remainders need removal.
  foreach ($files as $fid => $count) {
    $file = file_load($fid);
    file_usage_delete($file, 'css_injector_images');
    file_usage_delete($file, 'imce');
    drupal_set_message(t('%filename has been removed from the CSS Injector repository.', array('%filename' => $file->filename)));
  }
}

/**
 * q = admin/config/development/css-injector/imce.
 */
function css_injector_images_imce_user_page($user = FALSE) {
  if ($user === FALSE) {
    global $user;
    $account = user_load($user->uid);
  }
  return theme('css_injector_images_imce_user_page', array('account' => $account));
}

/**
 * Checks access to user/{$account->uid}/imce for the $user.
 */
function css_injector_images_imce_page_access($user = FALSE) {

  if ($user === FALSE) {
    global $user;
    $account = user_load($user->uid);
  }

  if ($account->uid == 1) {
    return TRUE;
  }

  $profile = imce_user_profile($account);

  if ($profile) {
    if (isset($profile['ciitab'])) {
      if ($profile['ciitab']) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * Intended to run before IMCE's imce_delete_submit function.
 * Removes the selected file(s) from the usage management enabling deletion
 */
function css_injector_images_imce_delete_submit($form, &$form_state) {
  foreach ($form_state['values']['filenames'] as $filename) {
    $imce = $form_state['build_info']['args'][0]['imce'];
    $uri = imce_dir_uri($imce);
    if ($uri == variable_get('css_injector_images_upload_location')) {
      $file = file_load_multiple(array(), array('uri' => $uri . $filename));
      $file = reset($file);
      if ($file) {
        $usage = file_usage_list($file);
        if (isset($usage['css_injector_images'])) {
          file_usage_delete($file, 'css_injector_images', 'file');
          drupal_set_message(t('%filename has been removed from the CSS Injector repository.', array('%filename' => $file->filename)));
        }
      }
    }
  }
}

/**
 * Intended to run after IMCE's imce_upload_submit function.
 * Adds the selected file(s) to the usage management enabling css injector helper integration
 */
function css_injector_images_imce_upload_submit($form, &$form_state) {

  $imce = $form_state['build_info']['args'][0]['imce'];
  $uri = imce_dir_uri($imce);

  if ($uri == variable_get('css_injector_images_upload_location')) {
    if (isset($imce['added'][0]['id'])) {
      $file = file_load(intval($imce['added'][0]['id']));
      $extensions = str_replace(' ', '|', variable_get('css_injector_images_upload_validators'));
      if (!empty($extensions) && preg_match('/\.(' . $extensions . ')$/i', $file->filename)) {
        file_usage_add($file, 'css_injector_images', 'file', 0);
        drupal_set_message(t('%filename has been added to the CSS Injector repository.', array('%filename' => $file->filename)));
      }
    }
  }
}

/**
 *
 */
function css_injector_images_theme_registry_alter(&$registry) {
  if (isset($registry['imce_file_list'])) {
    // overriding theming to add the filepath info
    $registry['imce_file_list']['theme path'] = drupal_get_path('module', 'css_injector_images');
    $registry['imce_file_list']['path'] = $registry['imce_file_list']['theme path'] . '/tpl';
  }
}

/**
 * Implements hook_theme().
 */
function css_injector_images_theme() {
  $path = drupal_get_path('module', 'css_injector_images') . '/tpl';
  $theme['css_injector_images_imce_user_page'] = array(
    'variables' => array('account' => NULL),
  );
  return $theme;
}

/**
 * Returns the html for user's file browser tab.
 */
function theme_css_injector_images_imce_user_page($variables) {

  global $user;

  $account = $variables['account'];
  $options = array();

  // switch to account's active folder
  if ($user->uid == 1 && $account->uid != 1) {
    require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'imce') . '/inc/imce.page.inc';
    $imce = imce_initiate_profile($account);
    $options['query'] = array('dir' => $imce['dir']);
  }

  return '<iframe src="' . url('imce', $options) . '" frameborder="0" style="border: 1px solid #eee; width: 99%; height: 520px" class="imce-frame"></iframe>';
}

//EOF
